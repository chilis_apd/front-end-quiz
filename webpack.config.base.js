const path = require('path');
const fs = require('fs-extra');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');




const htmlPlugins = fs.readdirSync('./src/views/pages').filter((file) => {
	return file.substr(-4) == '.hbs';
}).map((filename) => {
	let pageName = filename.substring(0, filename.length - 4);
	return new HtmlWebpackPlugin({
		template: `src/views/pages/${pageName}.hbs`,
		filename: `${pageName}.html`,
		minify: false,
	});
});

module.exports = {
	entry: './src/scripts/entry.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'js/bundle.js',
	},
	resolve: {
		modules: [
			'scripts',
			'node_modules'
		],
		extensions: ['.js', '.json','.css','.scss'],
		alias: {
			'modules': 'modules',
			'styles': '../scss',
			'handlebars': 'handlebars/dist/handlebars.js'
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							compact: false,
							presets: ['es2015']
						}
					}
				],
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: 'html-loader',
						options: {

						}
					}
				],
			},
			{
				test: /\.hbs$/,
				use: [
					{
						loader: 'handlebars-loader',
						options: {
							partialDirs: path.resolve(__dirname, 'src/views/partials/'),
							precompileOptions: {
								knownHelpersOnly: false,
							},
							// inlineRequires: '/img/',
							// rootRelative: './src/views/',
						}
					}
				],
			},
			{
				test: /\.(eot|ttf|woff|woff2|svg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'resources/fonts/',
							publicPath: '../resources/fonts/',
						}
					}
				],
			},
			{
				test: /\.(jpg|jpeg|png|gif|svg)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'resources/img/',
							publicPath: '../resources/img',
						}
					}
				],
			},
		],
	},
	plugins: [
		new CleanWebpackPlugin(['dist']),
		...htmlPlugins,
	],
	node: {
		console: true,
		fs: 'empty',
		net: 'empty',
		tls: 'empty',
		dns: 'empty'
	}
};
