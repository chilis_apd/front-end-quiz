Maxis front-end development
========================================

## Features
- Develop with css preprocessor SASS
- Ensure consistent css code styling with stylelint
- Use [Handlebars.js] to keep HTML organized into templates and partials
- Local development server with live reload


## Requirements
- Nodejs (https://nodejs.org/en/) installed on local machine with at least version 7.
- Have gulp installed globally on the local machine with: `npm i -g gulp`.

## Important
- The folder 'js' is use to contain 3rd-party library files and therefore will not be processed by webpack.
- The foldder 'scripts' is used to contain a webpack entry file - 'entry.js' and potentially other js dependency module. This will be processed by webpack.


## Commands

### First time
```
npm install
```

### Development
```
npm run dev
```


### Build
Generate static files. Usually you don't have to use this command.
```
npm run build
```

### Deployment

### Deploy to AEM folder structure
Generate static files and copy files over to their corresponding AEM folder structure.
```
npm run deploy
```

