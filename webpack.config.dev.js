const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.config.base.js');
const webpack = require('webpack');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = merge(baseConfig, {
	devtool: 'eval-source-map',

	devServer: {
		port: '8081',
		// hot: true,
		// historyApiFallback: {
		// 	rewrites: [
		// 		{ from: /^\/$/, to: 'src' },
		// 	],
		// },
		contentBase: path.join(__dirname, 'src/'),
	},

	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					'style-loader?sourceMap=true',
					'css-loader?sourceMap=true',
					'postcss-loader?sourceMap=true',
					{
						loader: 'sass-loader',
						options: {
							indentWidth: 4,
							outputStyle: 'expanded',
							sourceMap: true,
						}
					},
				],
			},
		],
	},
	plugins: [
		// new webpack.NamedModulesPlugin(),
		// new webpack.HotModuleReplacementPlugin(),
		new StyleLintPlugin({
			configFile: './stylelint.config.js',
			syntax: 'scss',
		}),
	],
});

  
