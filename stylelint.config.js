module.exports = {
    extends: "stylelint-config-recommended-scss",
    rules: {
        'block-no-empty': null,
        'function-whitespace-after': 'always',
        'number-leading-zero': 'always',
        'unit-case': 'lower',
        'value-keyword-case': 'lower',
        'value-list-comma-space-before': 'never',
        'value-list-comma-space-after': 'always',
        'property-case': 'lower',
        'declaration-bang-space-before': 'always',
        'declaration-bang-space-after': 'never',
        'declaration-colon-space-before': 'never',
        'declaration-colon-space-after': 'always',
        'declaration-block-trailing-semicolon': 'always',
        'selector-attribute-quotes': 'always',
        'media-feature-colon-space-before': 'never',
        'media-feature-colon-space-after': 'always',
        'media-feature-name-case': 'lower',
        'font-family-no-missing-generic-family-keyword' : null,
        'no-descending-specificity' : null
    },
};