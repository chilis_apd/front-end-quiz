// global var
import jQuery from "jquery"

window.$ = window.jQuery = jQuery
var _ = require('lodash')

// styles
import 'styles/main.scss'
import 'bootstrap-select/sass/bootstrap-select.scss';

// js
import 'bootstrap'


// imports
import dropdown from 'modules/dropdowns.js'
import cardrequest from 'modules/aiamain.js'


// libs


// run module
cardrequest.init()
