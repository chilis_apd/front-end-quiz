import $ from 'jquery'

function init() { 
    populateUsers()
    dropdownPopulate()
    dropdownUserFilter()
    dateConvertion()
}


const userCardArticles = (arr) => {
    const template = arr.map(a => `
    <article class="card-list-item" data-user-id="${a.id}">
        <div class="card-item-img-container">
            <img src="img/userphoto-prime1.png">
        </div>
        <div class="card-item-desc-container">
            <h4 class="item-desc-title"> ${a.name} </h4>
            <ul class="item-desc-dets">
                <li>
                    Email: <strong>${a.email}</strong>
                </li>
                <li>
                    Mobile: <strong>${a.phone}</strong>
                </li>
                <li> Company: <strong>${a.company.name}.</strong>
                </li>
                <li>
                    Address: <strong>${a.address.street}, ${a.address.suite}, ${a.address.city}, ${a.address.zipcode}, (${a.address.geo.lat}, ${a.address.geo.lng})</strong>
                </li>
                <li>
                    Website: <strong>${a.website}</strong>
                </li>
            </ul>
        </div>
    </article>
    `)
    return template
}

function populateUsers() {
    // onload user functions to populate on page load
    var $jsonApi = 'https://jsonplaceholder.typicode.com/users';

        $.ajax({
            type: 'GET',
            url: $jsonApi,
            success: function(data) {
                const arr = data         
                $('.js-user-container').html(userCardArticles(arr))
            }
        })
}

function dropdownPopulate() {
    // onload to populate data from dropdown filter on page load
    var $jsonApi = 'https://jsonplaceholder.typicode.com/users';
    var $dropdownUser = $('.js-card-filter-dropdown').find(':selected');

    const dropdowntemp = (arr) => {
        const template = arr.map(a => `
                <option value="${a.id}">${a.name}</option>
            `)
            $dropdownUser.after(template)
            $dropdownUser.selectpicker('refresh')
    }

        $.ajax({
            type: 'GET',
            url: $jsonApi,
            success: function(data) {
                const arr = data
                dropdowntemp(arr)
            }
        })
}

function dropdownUserFilter() {
    // onchange to request in API and filter the user's card
    var $dropdownUser = $('.js-card-filter-dropdown');
    var $jsonApi = 'https://jsonplaceholder.typicode.com/users';

    $dropdownUser.on('change.bs.select', function() {
        $.ajax({
            type: 'GET',
            url: $jsonApi + '?userName=' + $dropdownUser.val(),
            success: function(data) {
                const arr = data         
                $('.js-user-container').html(userCardArticles(arr))
            }
        })
    })
}

function dateConvertion() {
    var $btnConvert = $('.js-convert-date');

    $btnConvert.on('click', function() {
        var inputDate = $('[name="date"]').val()
        var myDate = inputDate.split("-").reverse().reverse().join("-");
        $('.date-converted').html(myDate)
    })
}


export default { init }