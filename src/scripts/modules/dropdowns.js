import 'bootstrap-select'


const selectOthersHandlr = (el, val) => {
    const d = $(el).closest('.dropdown-select.form-group').next('.mxs-dd-others')
    if (val === "others") {
        d.removeClass('hidden')
        if($(el).attr('required')) {
            d.find('[name="others"]').prop('required','required')
        }
    } else {
        d.find('[name="others"]').removeAttr('required')
        d.find('[name="others"]').val('')
        d.addClass('hidden')
        d.removeClass('invalid')
    }
}

function init() {
    $(document).ready(function(){
        $('select').selectpicker()
        $('select').on('changed.bs.select', function (e) {
            selectOthersHandlr(this, e.target.value)
        })
    })
}



export default { init }