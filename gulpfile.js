const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const fs = require('fs');
const clientlibsFolder = '../../../../../apps/src/main/content/jcr_root/apps/mxs/clientlibs';


// ===================================== css =====================================
gulp.task('css', () => {
    return gulp.src('dist/css/**/*')
        .pipe(gulp.dest(`${clientlibsFolder}/main/css`));
});
gulp.task('css-minify', () => {
    return gulp.src('dist/css/**/*')
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min',
        }))
        .pipe(gulp.dest(`${clientlibsFolder}/main/css`));
});


// ===================================== js (js) =====================================
gulp.task('js', () => {
    return gulp.src('dist/js/**/*').pipe(gulp.dest(`${clientlibsFolder}/main/js`));
});


// ===================================== bundle.js minify =====================================
gulp.task('bundle-js-minify', () => {
    return gulp.src('dist/js/bundle.js')
        .pipe(uglify({
            compress: {
                drop_console: true
            }
        }))
        .pipe(rename({
            suffix: '.min',
        }))
        .pipe(gulp.dest(`${clientlibsFolder}/main/js`));
});



// ===================================== fonts =====================================
gulp.task('fonts', () => {
    return gulp.src('dist/resources/fonts/**/*').pipe(gulp.dest(`${clientlibsFolder}/main/resources/fonts`));
});



// ===================================== images =====================================
gulp.task('images', () => {
    return gulp.src('dist/resources/img/**/*').pipe(gulp.dest(`${clientlibsFolder}/main/resources/img`));
});


gulp.task('test-comp',() =>{
    console.log("Running Component Tester");


    var newest = null;
    var dir = './src/views/partials/';
    var files = fs.readdirSync(dir);
    one_matched = 0

    for (i = 0; i < files.length; i++) {

        if (one_matched == 0) { //take first file
            newest = dir +  files[i];
            one_matched = 1;
            continue;
        }

        var f1_time = fs.statSync(dir + files[i]).mtime.getTime();
        var f2_time = fs.statSync(newest).mtime.getTime();

        if (f1_time > f2_time) {
            newest = dir + files[i];
        }

    }

    if (newest != null)
        console.log ('Component tested using latest timestamp: ' + newest);
    else{
        console.log('newest file not found');
    }

    var fileContent = fs.readFileSync(newest, 'utf8');
    fs.writeFile(dir + '_test-injection.hbs', fileContent);

});


gulp.task('deploy', [
    // 'libs',
    'css',
    'css-minify',
    'js',
    'bundle-js-minify',
    'fonts',
    'images',
], () => {
    
});



